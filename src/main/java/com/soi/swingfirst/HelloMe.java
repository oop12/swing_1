/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.swingfirst;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author user
 */
public class HelloMe {
    
    public static void main(String[] args) {
        JFrame frameMain = new JFrame("Hello Me");
        frameMain.setSize(500, 300);
        frameMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel lblYourName = new JLabel("Your name: ");
        lblYourName.setSize(70, 20);
        lblYourName.setLocation(5, 5);
        lblYourName.setBackground(Color.green);
        lblYourName.setOpaque(true);
        
        JTextField txtYourName = new JTextField();
        txtYourName.setSize(200, 20);
        txtYourName.setLocation(80, 5);
        
        JButton btnHello = new JButton("HELLO");
        btnHello.setSize(80, 20);
        btnHello.setLocation(170, 50);
        
        JLabel lblHello = new JLabel("Hello...", JLabel.CENTER);
        lblHello.setSize(200, 20);
        lblHello.setLocation(170, 90);
        lblHello.setBackground(Color.green);
        lblHello.setOpaque(true);
        lblHello.setFont(new Font("Verdana", Font.PLAIN, 14));
        
        frameMain.setLayout(null);
        
        btnHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = txtYourName.getText();
                lblHello.setText("HELLO " + name);                
            }            
        });
        
        frameMain.add(lblYourName);
        frameMain.add(txtYourName);
        frameMain.add(btnHello);
        frameMain.add(lblHello);
        
        frameMain.setVisible(true);
        
    }
    
}
